from django.contrib import admin

from reader.models import  Author, Book, Reader, Profile
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

class AuthorAdmin(admin.ModelAdmin):
    list_display = ('first_name','email')
    search_fields = ['first_name']
    list_filter = ['first_name']

class ReaderAdmin(admin.ModelAdmin):
    #filter_horizontal = ['book'] 
    filter_vertical = ['book'] 

class ProfileAdmin(BaseUserAdmin):
    pass

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book)
admin.site.register(Reader, ReaderAdmin)
admin.site.register(Profile)