"""main URL Configuration

"""
from django.conf.urls import include, url
from django.contrib import admin

from reader import views
from reader.views import get_author_filter
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
	
	url(r'^home/', views.HomeView.as_view()),
	url(r'^regist/', views.Regist.as_view(), name = 'regist'),
    url(r'^login/$',  views.Login.as_view()),
    url(r'^logout/$',  views.LogOut.as_view()),
   # url(r'^accounts/logout/$', logout),
	#url(r'^get_author/$', get_author_filter),
	url(r'^get_author/$', views.AuthorView.as_view(), name = 'get_author'),
	url(r'^get_author/(?P<pk>\d+)/$', views.GetAuthorView.as_view(), name= 'get_view'),
	url(r'^get_list/$', views.GetAuthorList.as_view(), name = 'get_list'),
	url(r'^get_save/$', views.GetAuthorSave.as_view(), name = 'get_save'),
	url(r'^avatar/$', views.Avatar.as_view(), name = 'avatar'),
	
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)