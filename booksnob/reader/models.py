from django.db import models
from reader.managers import AuthorManager
from django import forms
from django.contrib.auth.models import User

class Author(models.Model):
	first_name = models.CharField(max_length=30)
	last_name = models.CharField(max_length=40)
	email = models.EmailField()
	filter_author = AuthorManager()
	objects = models.Manager()

	def __unicode__(self):
		return '{0} {1} {2}'.format(self.first_name, self.last_name, self.email)

class Book(models.Model):
	author = models.ForeignKey(Author)
	name = models.CharField(max_length=100)
	
	def __unicode__(self):
		return '{0} - {1}'.format(self.author, self.name)

class Reader(models.Model):
	name = models.CharField(max_length = 100)
	book = models.ManyToManyField(Book)

	def __unicode__(self):
		return '{0} {1}'.format(self.name, self.book)

class Profile(models.Model):
	user = models.OneToOneField(User)
	avatar = models.ImageField(upload_to = '/static/')

	def __unicode__(self):
		return '{0} {1}'.format(self.user, self.avatar)
