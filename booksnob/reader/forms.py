from django import forms
from models import Author, Profile
from django.contrib.auth.forms import UserCreationForm

class AuthorForm(forms.ModelForm):
	class Meta:
		model = Author
		fields = '__all__'

class UserForm(UserCreationForm):

    class Meta:
        model = Profile
        fields = '__all__'