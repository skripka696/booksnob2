# -*- coding: utf-8 -*-
from django.shortcuts import render, render_to_response, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import TemplateView, DetailView, ListView
from django.views.generic.edit import FormView, View
from reader.models import  Author, Book, Reader,Profile
from reader.actions import get_books,get_author,get_list
from forms import AuthorForm
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import login, logout

from reader.forms import UserForm


class HomeView(TemplateView):
    template_name ='reader/home.html'
    #template_name ='testapp/home2.html'
    #template_name ='testapp/home3.html'
    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        context['author_name'] = get_author()
            

        context['books'] = get_books()
        context['list'] = get_list()

        return context

def get_author_filter(request):
    #Author.filter_author.get_author()
    #if request.POST:
    form = AuthorForm()
    return HttpResponse(form)

class AuthorView(FormView):
    form_class = AuthorForm
    template_name = 'reader/author.html'
    success_url = '/critic/thanks'
    #def post(self, request,*args,**kwargs):
    def form_valid(self,form):
        self.object = form.save()
        print self.object
        return super(AuthorView, self).form_valid(form)

class GetAuthorView(DetailView):
    model = Author
    template_name = 'reader/author_view.html'

    # def get(self, request, *args, **kwargs):
        # print 1


class GetAuthorList(ListView):
    model = Author
    template_name = 'reader/author_view_list.html'
    
    def post(self,request,*args,**kwargs):
        q = request.POST.get('search','Bill')
        u = request.POST.get('up','')
              
        search = Author.objects.filter(first_name__icontains=q)
        if u =='up':
            search = search.order_by('id')
        else:
            search = search.order_by('-id')
        # up = Author.objects.filter(first_name__icontains=q).order_by('first_name')
        # down = Author.objects.filter(first_name__icontains=q).order_by('-first_name')
        #search = Author.objects.filter(first_name__istartswith=q)
        return render(request,self.template_name,
            {'author_list': search})


class GetAuthorSave(FormView):
    form_class = AuthorForm
    model = Author
    template_name = 'reader/save.html'
    success_url = '/get_save/'
    
    def form_valid(self,form):
        self.object = form.save()
        print self.object
        return super(GetAuthorSave, self).form_valid(form)


class Regist(FormView):
    form_class = UserCreationForm
    success_url = '/get_save/'
    template_name = 'reader/regist.html'

    def form_valid(self,form):
        self.object = form.save()
        return super(Regist, self).form_valid(form)

class Login(FormView):
    form_class = AuthenticationForm
    success_url = '/get_list/'
    template_name = 'reader/login.html'

    def form_valid(self,form):
        user = form.get_user()
        login(self.request,user)
        return super(Login, self).form_valid(form)


class LogOut(View):
    success_url = '/regist/'
    template_name = 'reader/base.html'

    def get(self,request):
        logout(request)
        return HttpResponseRedirect('/login')
'''    
@login_required
def EditProfile(request):
    user = request.user
    if request.method == "POST":
        usrform = UserForm(data=request.POST, instance=user)
        proform = ProfileForm(data=request.POST, instance=user.get_profile())
        if usrform.is_valid() and proform.is_valid():
            user = usrform.save()
            profile = proform.save(commit=False)
            profile.user = user
            profile.save()
            return HttpResponseRedirect('/avatar/')
        else:
            return render_to_response('/get_list/', {
                'profile': request.user.get_profile,
                'usrform': usrform,
                'proform': proform},
                context_instance=RequestContext(request))
    else:
        usrform = UserForm(instance=user)
        proform = ProfileForm(instance=user.get_profile() )
        profile = request.user.get_profile
        return render(request, "/get_list/", {
            'profile': profile,
            'usrform': usrform,
            'proform': proform,
            })
'''
#@login_required
class Avatar(FormView):

    def get(self, request):
        form = UserForm()
        return render(request, 'reader/avatar.html', {'form': form})

    def post(self, request):
        #username = request.POST['user']
       # password = request.POST['password']
        form = UserForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/critic/thanks/')
        return render(request, 'reader/avatar.html', {'form': form})